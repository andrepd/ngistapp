package br.com.netshoes.ngist;

import org.junit.Test;

import java.util.List;

import br.com.netshoes.ngist.model.Gist;
import br.com.netshoes.ngist.services.GistService;
import br.com.netshoes.ngist.services.GistServiceFactory;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;

import static org.junit.Assert.assertEquals;


public class ServiceTeste {
    @Test
    public void testeService() throws Exception {

        MockWebServer server = new MockWebServer();

        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody("[]"));

        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody("[{ \"id\": \"fb21145b21025a411150c158df979221\",  \n" +
                        "\"files\":{\n" +
                        "  \"README.md\": {\n" +
                        "\"filename\": \"README.md\",\n" +
                        "\"type\": \"text/plain\",\n" +
                        "\"language\": \"Markdown\",\n" +
                        "\"raw_url\": \"https://gist.githubusercontent.com/mayblue9/fb21145b21025a411150c158df979221/raw/cc2be79557e08f922a406cbe5fd9f7c8dd436185/README.md\",\n" +
                        "\"size\": 43\n" +
                        "  },\n" +
                        "  \"index.html\":{\n" +
                        "\"filename\": \"index.html\",\n" +
                        "\"type\": \"text/html\",\n" +
                        "\"language\": \"HTML\",\n" +
                        "\"raw_url\": \"https://gist.githubusercontent.com/mayblue9/fb21145b21025a411150c158df979221/raw/8a5ff84b342c78c8305f2682f7a105b4a6a49e1f/index.html\",\n" +
                        "\"size\": 3302\n" +
                        "  }\n" +
                        "},\"description\": \"k-means + d3.js\",    \n" +
                        "\"owner\": {\n" +
                        "  \"login\": \"mayblue9\",      \n" +
                        "  \"avatar_url\": \"https://avatars.githubusercontent.com/u/14083532?v=3\" \n" +
                        "}}]"));
        server.start();

        GistService service = GistServiceFactory.createService(server.url("/").toString());

        Call<List<Gist>> call = service.getGistsByDate(null);
        List<Gist> gists = call.execute().body();
        assertEquals(gists.size(), 0);

        Call<List<Gist>> call2 = service.getGistsByPage(1);
        gists = call2.execute().body();

        assertEquals(gists.size(), 1);
        assertEquals(gists.get(0).getFiles().size(), 2);
        assertEquals(gists.get(0).getOwner().getLogin(), "mayblue9");
        assertEquals(gists.get(0).getLanguage(), "Markdown");

        server.shutdown();
    }
}