package br.com.netshoes.ngist.view.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.netshoes.ngist.R;
import br.com.netshoes.ngist.model.Gist;
import br.com.netshoes.ngist.model.GistFile;
import br.com.netshoes.ngist.view.adapters.GistFileAdapter;

/**
 *
 * Activity com o detalhamento do Gist
 *
 * Created by André on 31/07/2016.
 *
 */
@EActivity(R.layout.activity_gist)
public class GistActivity extends AppCompatActivity {

    @Extra
    protected Gist gist;

    @ViewById
    protected ImageView imgOwnerAvatar;

    @ViewById
    protected TextView txtOwnerName;

    @ViewById
    protected RecyclerView listFiles;

    @Bean
    protected GistFileAdapter adapter;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @AfterViews
    protected void afterViews(){

        //Configuração do RecyclerView
        listFiles.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listFiles.setLayoutManager(layoutManager);

        if (gist != null ) {

            if (gist.getOwner() != null) {
                Picasso.with(this)
                        .load(gist.getOwner().getAvatarUrl())
                        .placeholder(R.drawable.anonymous_avatar)
                        .into(imgOwnerAvatar);
                txtOwnerName.setText(gist.getOwner().getLogin());
            } else {
                txtOwnerName.setText(R.string.anonymous_name);
            }

            if (gist.getFiles() != null) {
                List<GistFile> files = new ArrayList<>();
                files.addAll(gist.getFiles().values());
                adapter.setFiles(files);
                listFiles.setAdapter(adapter);
            }
        }

    }
}
