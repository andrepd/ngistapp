package br.com.netshoes.ngist.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.netshoes.ngist.utils.DateUtils;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 *
 * Classe de configuração do retrofit para acesso a API
 *
 * Created by André on 30/07/2016.
 */
public class GistServiceFactory {

    private static final String API_BASE_URL = "https://api.github.com/gists/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
    private static Retrofit.Builder builder = new Retrofit.Builder();
    private static ObjectMapper mapper = new ObjectMapper();

    static{

        //Configuração do nível de log
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        httpClient.addInterceptor(logging);

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setDateFormat(DateUtils.getDefaultDateFormat());

        builder = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .client(httpClient.build());
    }

    /**
     *
     * Cria uma nova instância do serviço e retorna
     *
     * @return GistService
     */
    public static GistService createService() {
        Retrofit retrofit = builder.baseUrl(API_BASE_URL).build();
        return retrofit.create(GistService.class);
    }

    public static GistService createService(String url) {
        Retrofit retrofit = builder.baseUrl(url).build();
        return retrofit.create(GistService.class);
    }
}
