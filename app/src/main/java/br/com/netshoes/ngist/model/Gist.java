package br.com.netshoes.ngist.model;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * Modelo do Gist
 *
 * Created by André on 27/07/2016.
 */
public class Gist implements Serializable{

    private String id;
    private String description;
    private Owner owner;
    private Map<String, GistFile> files;

    public String getType(){
        GistFile first = getFirstFile();
        if (first != null) return first.getType();
        return "";
    }

    public String getLanguage(){
        GistFile first = getFirstFile();
        if (first != null) return first.getLanguage();
        return "";
    }

    private GistFile getFirstFile(){
        if (files != null && !files.isEmpty()) return files.values().iterator().next();
        return null;
    }

    public Map<String, GistFile> getFiles() {
        return files;
    }

    public void setFiles(Map<String, GistFile> files) {
        this.files = files;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }


    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Gist))
            return false;

        if (obj == this)
            return true;

        if (((Gist) obj).getId() == this.id)
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}
