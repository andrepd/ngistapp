package br.com.netshoes.ngist.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import br.com.netshoes.ngist.model.Gist;
import br.com.netshoes.ngist.view.custom.GistsListItem;
import br.com.netshoes.ngist.view.custom.GistsListItem_;

/**
 *
 * Adapater para lista de Gits
 *
 * Created by André on 28/07/2016.
 */
@EBean
public class GistAdapter extends RecyclerView.Adapter<ViewWrapper>{

    @RootContext
    Context context;

    private List<Gist> gists = new ArrayList<>();

    private OnItemClickListener onItemClickListener;

    public void setGists(List<Gist> gists) {
        this.gists = gists;
    }

    /**
     * Adiciona um listener ao evento do click do item da lista
     *
     * @param onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewWrapper onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper(GistsListItem_.build(context));
    }

    @Override
    public void onBindViewHolder(ViewWrapper holder, int position) {
        final Gist gist = gists.get(position);
        ((GistsListItem)holder.getView()).bind(gist);
        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onItemClickListener !=null) onItemClickListener.onItemClick(gist);
            }
        });
    }

    /**
     * Adiciona  items ao fim da lista
     *
     * @param newGists
     */
    public void addGists(List<Gist> newGists){
        int start = gists.size();
        gists.addAll(newGists);
        notifyItemRangeInserted(start, newGists.size());
    }

    /**
     * Adiciona item ao início da lista
     *
     * @param newGists
     */
    public void addGistsAtStart(List<Gist> newGists){
        gists.addAll(0, newGists);
        notifyItemRangeInserted(0, newGists.size());
    }

    @Override
    public int getItemCount() {
        return gists.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Gist item);
    }


}
