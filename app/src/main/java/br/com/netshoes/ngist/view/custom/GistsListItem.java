package br.com.netshoes.ngist.view.custom;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.netshoes.ngist.R;
import br.com.netshoes.ngist.model.Gist;

/**
 *
 * View do item da Lista de Gits
 *
 * Created by André on 27/07/2016.
 */
@EViewGroup(R.layout.gists_list_item)
public class GistsListItem extends RelativeLayout {

    private Context context;

    @ViewById
    ImageView imgOwnerAvatar;

    @ViewById
    TextView txtOwnerName;

    @ViewById
    TextView txtGistType;

    @ViewById
    TextView txtGistLanguage;

    public GistsListItem(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * Carrega valores do Gist nos campos
     *
     * @param gist
     */
    public void bind(Gist gist) {

        if (gist.getOwner() != null) {
            //Carrega o avatar com Picasso
            Picasso.with(context)
                    .load(gist.getOwner().getAvatarUrl())
                    .placeholder(R.drawable.anonymous_avatar)
                    .into(imgOwnerAvatar);
            txtOwnerName.setText(gist.getOwner().getLogin());
        } else {
            //Se não existir um owner usa um nome e imagem anonimo
            imgOwnerAvatar.setImageResource(R.drawable.anonymous_avatar);
            txtOwnerName.setText(R.string.anonymous_name);
        }

        txtGistType.setText(gist.getType());
        txtGistLanguage.setText(gist.getLanguage());
    }

}
