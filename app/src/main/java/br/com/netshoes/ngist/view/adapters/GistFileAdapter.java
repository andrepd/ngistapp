package br.com.netshoes.ngist.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.IOException;
import java.util.List;

import br.com.netshoes.ngist.R;
import br.com.netshoes.ngist.model.GistFile;
import br.com.netshoes.ngist.services.GistService;
import br.com.netshoes.ngist.services.GistServiceFactory;
import br.com.netshoes.ngist.view.custom.GistFileItem;
import br.com.netshoes.ngist.view.custom.GistFileItem_;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * Adapter para lista de arquivos
 *
 * Created by André on 31/07/2016.
 */
@EBean
public class GistFileAdapter extends RecyclerView.Adapter<ViewWrapper> {

    private static final String TAG = GistFileAdapter.class.getSimpleName();

    @RootContext
    Context context;

    private List<GistFile> files;

    private GistService service;

    public GistFileAdapter(){
        service = GistServiceFactory.createService();
    }

    public void setFiles(List<GistFile> files){
        this.files = files;
    }

    @Override
    public ViewWrapper onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper(GistFileItem_.build(context));
    }

    @Override
    public void onBindViewHolder(ViewWrapper holder, int position) {

        GistFile file = files.get(position);
        final GistFileItem fileItem = (GistFileItem) holder.getView();

        fileItem.bind(file);
        Call<ResponseBody> call = service.downloadFile(file.getRawUrl());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        fileItem.setFileContent(response.body().string());
                    } catch (IOException e) {
                        fileItem.setFileContent(context.getString(R.string.file_error_msg));
                        Log.e(TAG, context.getString(R.string.file_error_msg), e);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, context.getString(R.string.file_error_msg), t);
                fileItem.setFileContent(context.getString(R.string.file_error_msg));
            }
        });
    }

    @Override
    public int getItemCount() {
        return files.size();
    }
}
