package br.com.netshoes.ngist.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * Classe auxiliar para formatação de Data
 *
 * Created by André on 31/07/2016.
 */
public class DateUtils {

    private static final String DEFAULT_DATE_PATTERN  = "yyyy-MM-dd'T'HH:mm:ssz";
    private static SimpleDateFormat dateFormat;

    static{
        dateFormat = new SimpleDateFormat(DEFAULT_DATE_PATTERN, Locale.getDefault());
    }

    public static String format(Date date){
        if (date != null) return dateFormat.format(date);
        return null;
    }

    public static DateFormat getDefaultDateFormat(){
        return dateFormat;
    }

}
