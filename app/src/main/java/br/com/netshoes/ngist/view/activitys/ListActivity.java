package br.com.netshoes.ngist.view.activitys;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;
import java.util.List;

import br.com.netshoes.ngist.R;
import br.com.netshoes.ngist.model.Gist;
import br.com.netshoes.ngist.services.GistService;
import br.com.netshoes.ngist.services.GistServiceFactory;
import br.com.netshoes.ngist.utils.DateUtils;
import br.com.netshoes.ngist.view.adapters.GistAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * Activity principal com a lista de todos os Gists
 *
 * Created by André on 27/07/2016.
 */
@EActivity(R.layout.activity_list)
public class ListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    @ViewById
    RecyclerView listGists;

    @ViewById
    SwipeRefreshLayout swipeRefresh;

    @Bean
    GistAdapter adapter;

    private LinearLayoutManager layoutManager;
    private GistService service;
    private int page = 2;
    private Calendar lastUpdate = null;

    /**
     * Configura as view e faz o carregamento inicial dos items pelo serviço
     */
    @AfterViews
    protected void afterViews(){

        //Configuração do RecyclerView
        listGists.setAdapter(adapter);
        listGists.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listGists.setLayoutManager(layoutManager);

        listGists.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (adapter.getItemCount() == layoutManager.findLastCompletelyVisibleItemPosition() + 1){
                    loadGistsAtEnd();
                }
            }
        });

        adapter.setOnItemClickListener(new GistAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Gist item){
                GistActivity_.intent(ListActivity.this).gist(item).start();
            }
        });

        swipeRefresh.setOnRefreshListener(this);

        service = GistServiceFactory.createService();
        loadGistsAtStart();

    }

    @Override
    public void onRefresh() {
        loadGistsAtStart();
    }

    /**
     *  Carrega os Gits mais recentes do serviço e adiciona ao início da lista
     *
     */
    private void loadGistsAtStart(){

        swipeRefresh.setRefreshing(true);

        String since = null;
        if (lastUpdate != null){
            since = DateUtils.format(lastUpdate.getTime());
        }

        Call<List<Gist>> call = service.getGistsByDate(since);
        call.enqueue(new Callback<List<Gist>>() {
            @Override
            public void onResponse(Call<List<Gist>> call, Response<List<Gist>> response) {
                if (response.isSuccessful()){
                    adapter.addGistsAtStart(response.body());
                    swipeRefresh.setRefreshing(false);
                    lastUpdate = Calendar.getInstance();
                }else{
                    onServiceError();
                }
            }

            @Override
            public void onFailure(Call<List<Gist>> call, Throwable t) {
                onServiceError();
            }
        });

    }

    /**
     *
     * Carrega os Gists da próxima página e adiciona ao fim da lista
     *
     */
    private void loadGistsAtEnd(){

        swipeRefresh.setRefreshing(true);

        Call<List<Gist>> call = service.getGistsByPage(page);
        call.enqueue(new Callback<List<Gist>>() {
            @Override
            public void onResponse(Call<List<Gist>> call, Response<List<Gist>> response) {
                if (response.isSuccessful()){
                    adapter.addGists(response.body());
                    swipeRefresh.setRefreshing(false);
                    page++;
                }else{
                    onServiceError();
                }
            }

            @Override
            public void onFailure(Call<List<Gist>> call, Throwable t) {
                onServiceError();
            }
        });

    }

    /**
     *
     * Para a animação de carregamento e exibe uma mensagem de erro
     *
     */
    private void onServiceError(){
        swipeRefresh.setRefreshing(false);
        Toast.makeText(ListActivity.this, R.string.service_error_msg, Toast.LENGTH_LONG).show();
    }

}
