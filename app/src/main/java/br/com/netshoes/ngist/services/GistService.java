package br.com.netshoes.ngist.services;

import java.util.List;

import br.com.netshoes.ngist.model.Gist;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Declaração dos métodos do serviço
 *
 * Created by André on 30/07/2016.
 *
 */

public interface GistService {

    @GET("public")
    Call<List<Gist>> getGistsByPage(@Query("page") int page);

    @GET("public")
    Call<List<Gist>> getGistsByDate(@Query("since") String since);

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
