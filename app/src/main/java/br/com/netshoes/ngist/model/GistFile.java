package br.com.netshoes.ngist.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 *
 * Modelo dos arquivos de um Gist
 *
 * Created by André on 27/07/2016.
 */
public class GistFile implements Serializable {

    private String filename;
    private String type;
    private String language;
    @JsonProperty("raw_url")
    private String rawUrl;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRawUrl() {
        return rawUrl;
    }

    public void setRawUrl(String rawUrl) {
        this.rawUrl = rawUrl;
    }
}
