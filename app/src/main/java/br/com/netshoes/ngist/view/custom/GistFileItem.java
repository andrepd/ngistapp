package br.com.netshoes.ngist.view.custom;

import android.content.Context;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.netshoes.ngist.R;
import br.com.netshoes.ngist.model.GistFile;

/**
 * View do item da lista de arquivos
 *
 * Created by André on 31/07/2016.
 */
@EViewGroup(R.layout.file_list_item)
public class GistFileItem extends RelativeLayout{

    @ViewById
    protected TextView fileName;

    @ViewById
    protected TextView fileContent;

    @ViewById
    protected ProgressBar progressBar;

    public GistFileItem(Context context) {
        super(context);
    }

    /**
     * Carrega valores do Arquivo nos campos
     *
     * @param file
     */
    public void bind(GistFile file) {
        fileName.setText(file.getFilename());
    }

    /**
     * Carrega o conteúdo do arquivo
     *
     * @param content
     */
    public void setFileContent(String content){
        progressBar.setVisibility(GONE);
        fileContent.setText(content);
    }
}
