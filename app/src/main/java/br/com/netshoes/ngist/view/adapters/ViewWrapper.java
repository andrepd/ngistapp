package br.com.netshoes.ngist.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 *
 * Classe auxiliar pra injeção do view do item do adapter
 *
 * Created by André on 31/07/2016.
 */
public class ViewWrapper extends RecyclerView.ViewHolder {

    private View view;

    public ViewWrapper(View itemView) {
        super(itemView);
        this.view = itemView;
    }

    public View getView() {
        return view;
    }
}
